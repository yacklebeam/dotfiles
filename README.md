# Yacklebeam's dotfiles

These config files and scripts are the backbone of my Linux environments.

Currently centered around Arch Linux, though the config files should work in any distribution.

My preferred programs/tools (if config files aren't found above, I'm working on it...promise)
- Terminal: `st (suckless)`
- Shell: `zsh`, with `bash` available when needed
- Editor: `neovim (nvim)`
- File Manager: `vifm (vim file manager)`
- Window Manager: `i3wm (with gaps)`
- Status Bar: `i3-blocks`
