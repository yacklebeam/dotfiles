let mapleader=' '
syntax on

let g:vim_bootstrap_langs = "c,go"
let g:vim_bootstrap_editor = "nvim"
let vimplug_exists=expand('~/.config/nvim/autoload/plug.vim')

if !filereadable(vimplug_exists)
    if !executable("curl")
        echoerr "You have to install curl or first install vim-plug yourself!"
        execute "q!"
    endif
    echo "Installing Vim-Plug..."
    echo ""
    silent exec "!\curl -fLo " . vimplug_exists . " --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
    let g:not_finish_vimplug = "yes"

    autocmd VimEnter * PlugInstall
endif

call plug#begin(expand('~/.config/nvim/plugged'))

Plug 'morhetz/gruvbox'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'justinmk/vim-sneak'
Plug 'preservim/nerdcommenter'

Plug 'neovim/nvim-lspconfig'
Plug 'kabouzeid/nvim-lspinstall'
Plug 'hrsh7th/nvim-cmp'

Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-vsnip'

Plug 'hrsh7th/vim-vsnip'
Plug 'hrsh7th/vim-vsnip-integ'

Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-telescope/telescope-fzy-native.nvim'

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'blackCauldron7/surround.nvim'
Plug 'tikhomirov/vim-glsl'
"Plug 'nvim-lua/completion-nvim'
"Plug 'doronbehar/nvim-fugitive'

call plug#end()

let g:airline_section_z=''
let g:gruvbox_invert_selection='0'
colorscheme gruvbox

set nocompatible
set noerrorbells
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set smartcase
set ignorecase
set hidden
set incsearch
set nohlsearch
set ruler
set number
set rnu
set nowrap
set noswapfile
set nobackup
set undodir=~/.config/nvim/undodir
set undofile
set termguicolors
set scrolloff=8
set signcolumn=yes
" set colorcolumn=80
set path=.,,,**
set wildmenu
set wildignore=.git,tags
set mouse=nv
set noerrorbells
set cul

nnoremap <leader>h <C-w>h
nnoremap <leader>j <C-w>j
nnoremap <leader>k <C-w>k
nnoremap <leader>l <C-w>l

vnoremap <C-C> "*y
vnoremap <C-V> "*p

nnoremap <silent> <C-h> <C-w>h
nnoremap <silent> <C-l> <C-w>l
nnoremap <silent> <C-j> :cn<CR>
nnoremap <silent> <C-k> :cp<CR>
nnoremap <silent> <F12> :lua vim.lsp.buf.definition()<CR>
nnoremap <silent> <S-F12> :lua vim.lsp.buf.references()<CR>
nnoremap <silent> <F10> :lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> <F9> :lua vim.lsp.buf.hover()<CR>

nnoremap <silent> gd :lua vim.lsp.buf.definition()<CR>
nnoremap <silent> gD :lua vim.lsp.buf.implementation()<CR>

imap <expr> <C-j> vsnip#expandable() ? '<Plug>(vsnip-expand)' : '<C-j>'

command! Wa wa
command! W w
command! MakeTags !ctags -R .
command! Rename lua vim.lsp.buf.rename()

augroup TROXEL
    autocmd!
    autocmd QuickFixCmdPost [^l]* cwindow
    autocmd QuickFixCmdPost l* lwindow
    autocmd BufWritePre * :%s/\s\+$//e
    autocmd Filetype python setlocal makeprg=py\ %

    au BufNewFile,BufRead *.groff set filetype=groff
    autocmd Filetype groff setlocal makeprg=groff\ -ms\ -Tpdf\ %\ >\ %:r.pdf
augroup END

let g:completion_matching_strategy_list = ['exact', 'substring', 'fuzzy']
set completeopt=menu,menuone,noselect

lua << EOF

require'lspconfig'.clangd.setup {
    capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
}

require'cmp'.setup({
    snippet = {
        expand = function(args)
            vim.fn["vsnip#anonymous"](args.body)
        end,
    },
    mappings = {
        ['<C-Space>'] = require'cmp'.mapping.complete(),
        ['<C-e>'] = require'cmp'.mapping.close(),
        ['<CR>'] = require'cmp'.mapping.confirm({ select = true }),
    },
    sources = {
        { name = 'nvim_lsp' },
        --{ name = 'vsnip' },
        --{ name = 'buffer' },
        { name = 'path' },
    }
})
--require'lspconfig'.clangd.setup{ on_attach=require'compe'.on_attach }

--[[require'compe'.setup{
    enabled=true;
    autocomplete=true;
    debug = false;
    min_length = 1;
    preselect = 'enable';
    throttle_time = 80;
    source_timeout = 200;
    incomplete_delay = 400;
    max_abbr_width = 100;
    max_kind_width = 100;
    max_menu_width = 100;
    documentation = true;

    source={
    path = true;
    nvim_lsp = true;
    }
}]]

require'telescope'.setup{}

require'telescope'.load_extension('fzy_native')

require'surround'.setup{}

EOF

nnoremap <leader>fb :lua require('telescope.builtin').buffers()<CR>
nnoremap <C-b> :lua require('telescope.builtin').buffers()<CR>
nnoremap <leader>ff :lua require('telescope.builtin').find_files()<CR>
nnoremap <C-p> :lua require('telescope.builtin').find_files()<CR>
nnoremap <leader>fg :lua require('telescope.builtin').live_grep()<CR>
