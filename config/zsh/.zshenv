PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"
HISTFILE=~/.local/.zshhistory
HISTSIZE=999999999
SAVEHIST=999999999
TERMINAL=st
EDITOR=nvim
