setopt nomatch PROMPT_SUBST
unsetopt autocd beep extendedglob notify

# bindkey -v

zstyle :compinstall filename '/home/jtroxel/.config/zsh/.zshrc'
autoload -Uz compinit promptinit colors
compinit
promptinit
colors

parse_git_branch() {
    git symbolic-ref --short HEAD 2> /dev/null | awk '{ print " (" $0 ")" }'
}

PROMPT='[%~]%F{green}$(parse_git_branch)%f $ '

source $HOME/.aliases
